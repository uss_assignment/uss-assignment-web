/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 *
 * @author Afsara Tasnim Mim
 */
@Entity(name = "committee_lecturer")
public class CommitteeLecturer implements Serializable {

    @Id
    @ManyToOne
    @Cascade(CascadeType.ALL)
    private Lecturer lecturer;
    @Id
    @ManyToOne
    @Cascade(CascadeType.ALL)
    private Committee committee;
    @Id
    @ManyToOne
    @Cascade(CascadeType.ALL)
    private Faculty faculty;

    public CommitteeLecturer() {
    }

    public CommitteeLecturer(Lecturer lecturer, Committee committee, Faculty faculty) {
        this.lecturer = lecturer;
        this.committee = committee;
        this.faculty = faculty;
    }

    public Lecturer getLecturer() {
        return lecturer;
    }

    public void setLecturer(Lecturer lecturer) {
        this.lecturer = lecturer;
    }

    public Committee getCommittee() {
        return committee;
    }

    public void setCommittee(Committee committee) {
        this.committee = committee;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

}
