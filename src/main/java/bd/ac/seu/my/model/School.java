/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 *
 * @author Afsara Tasnim Mim
 */
@Entity(name = "school")
public class School implements Serializable {

    @Id
    private String schoolName;
    @ManyToOne(cascade = { CascadeType.ALL })
    private Faculty faculty;
    @ManyToOne(cascade = { CascadeType.ALL })
    private Campus campus;
    private String schoolBulding;
    

    public School() {
    }

    public School(String schoolName, Faculty faculty, Campus campus, String schoolBulding) {
        this.schoolName = schoolName;
        this.faculty = faculty;
        this.campus = campus;
        this.schoolBulding = schoolBulding;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public Campus getCampus() {
        return campus;
    }

    public void setCampus(Campus campus) {
        this.campus = campus;
    }

    public String getSchoolBulding() {
        return schoolBulding;
    }

    public void setSchoolBulding(String schoolBulding) {
        this.schoolBulding = schoolBulding;
    }

    

}
