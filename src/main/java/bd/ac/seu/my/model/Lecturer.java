/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 *
 * @author Afsara Tasnim Mim
 */
@Entity(name = "lecturer")
public class Lecturer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int staffId;
    @ManyToOne(cascade = { CascadeType.ALL })
    private School school;
    //@ManyToOne
    //@JoinTable(name = "lecturer_supervisor")
    //private List<Lecturer> supervisor;
    @ManyToOne(cascade = { CascadeType.ALL })
    private Lecturer supervisor;
    private String lecturereName;
    private String lecturertitle;
    private String officeRoom;

    public Lecturer() {
    }

    public Lecturer(int staffId, School school, Lecturer supervisor, String lecturereName, String lecturertitle, String officeRoom) {
        this.staffId = staffId;
        this.school = school;
        this.supervisor = supervisor;
        this.lecturereName = lecturereName;
        this.lecturertitle = lecturertitle;
        this.officeRoom = officeRoom;
    }
    
    
    
/*
    public Lecturer(School school, String lecturereName, String lecturertitle, String officeRoom) {
        this.school = school;
        this.lecturereName = lecturereName;
        this.lecturertitle = lecturertitle;
        this.officeRoom = officeRoom;
    }
*/
    public int getStaffId() {
        return staffId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }
/*
    public List<Lecturer> getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(List<Lecturer> supervisor) {
        this.supervisor = supervisor;
    }
*/

    public Lecturer getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(Lecturer supervisor) {
        this.supervisor = supervisor;
    }
    
    public String getLecturereName() {
        return lecturereName;
    }

    public void setLecturereName(String lecturereName) {
        this.lecturereName = lecturereName;
    }

    public String getLecturertitle() {
        return lecturertitle;
    }

    public void setLecturertitle(String lecturertitle) {
        this.lecturertitle = lecturertitle;
    }

    public String getOfficeRoom() {
        return officeRoom;
    }

    public void setOfficeRoom(String officeRoom) {
        this.officeRoom = officeRoom;
    }
    /*
    public void addSupervisor(Lecturer lecturer){
        if(supervisor == null)
            supervisor = new ArrayList<>();
        supervisor.add(lecturer);
    }
    */
}
