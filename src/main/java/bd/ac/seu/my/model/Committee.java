/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 *
 * @author Afsara Tasnim Mim
 */
@Entity(name = "committee")
public class Committee implements Serializable {

    @Id
    private String committeeTitle;
    @Id
    @ManyToOne
    @Cascade(CascadeType.ALL)
    private Faculty faculty;
    private String meetingFrequency;

    public Committee() {
    }

    public Committee(String committeeTitle, Faculty faculty, String meetingFrequency) {
        this.committeeTitle = committeeTitle;
        this.faculty = faculty;
        this.meetingFrequency = meetingFrequency;
    }

    public String getMeetingFrequency() {
        return meetingFrequency;
    }

    public void setMeetingFrequency(String meetingFrequency) {
        this.meetingFrequency = meetingFrequency;
    }

    public String getCommitteeTitle() {
        return committeeTitle;
    }

    public void setCommitteeTitle(String committeeTitle) {
        this.committeeTitle = committeeTitle;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

}
