/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 *
 * @author Afsara Tasnim Mim
 */
@Entity(name = "club")
public class Club implements Serializable {

    @Id
    private String clubName;
    @OneToOne(cascade = { CascadeType.ALL })
    private Campus campus;
    private String clubBuilding;
    private String clubPhoneNumber;

    public Club() {
    }

    public Club(String clubName, Campus campus, String clubBuilding, String clubPhoneNumber) {
        this.clubName = clubName;
        this.campus = campus;
        this.clubBuilding = clubBuilding;
        this.clubPhoneNumber = clubPhoneNumber;
    }

    public String getClubPhoneNumber() {
        return clubPhoneNumber;
    }

    public void setClubPhoneNumber(String clubPhoneNumber) {
        this.clubPhoneNumber = clubPhoneNumber;
    }

    public String getClubName() {
        return clubName;
    }

    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    public Campus getCampus() {
        return campus;
    }

    public void setCampus(Campus campus) {
        this.campus = campus;
    }

    public String getClubBuilding() {
        return clubBuilding;
    }

    public void setClubBuilding(String clubBuilding) {
        this.clubBuilding = clubBuilding;
    }

}
