/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 *
 * @author Afsara Tasnim Mim
 */
@Entity(name = "programme")
public class Programme implements Serializable {

    @Id
    private String programmeCode;
    @ManyToOne
    @Cascade(CascadeType.ALL)
    private School school;
    private String programmeTitle;
    private String programmeLevel;
    private String programmeLenth;

    public Programme() {
    }

    public Programme(String programmeCode, School school, String programmeTitle, String programmeLevel, String programmeLenth) {
        this.programmeCode = programmeCode;
        this.school = school;
        this.programmeTitle = programmeTitle;
        this.programmeLevel = programmeLevel;
        this.programmeLenth = programmeLenth;
    }

    public String getProgrammeLenth() {
        return programmeLenth;
    }

    public void setProgrammeLenth(String programmeLenth) {
        this.programmeLenth = programmeLenth;
    }

    public String getProgrammeCode() {
        return programmeCode;
    }

    public void setProgrammeCode(String programmeCode) {
        this.programmeCode = programmeCode;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public String getProgrammeTitle() {
        return programmeTitle;
    }

    public void setProgrammeTitle(String programmeTitle) {
        this.programmeTitle = programmeTitle;
    }

    public String getProgrammeLevel() {
        return programmeLevel;
    }

    public void setProgrammeLevel(String programmeLevel) {
        this.programmeLevel = programmeLevel;
    }

}
