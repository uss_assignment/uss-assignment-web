/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.Interface;

import bd.ac.seu.my.model.Club;
import java.util.List;

/**
 *
 * @author Afsara Tasnim Mim
 */
public interface ClubInterface {
    
    public List<Club> getClub();

    public void createClub(Club club);

    public void updateClub(Club club);

    public void deleteClub(Club club);
}
