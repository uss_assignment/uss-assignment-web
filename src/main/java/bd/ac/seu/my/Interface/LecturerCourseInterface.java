/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.Interface;

import bd.ac.seu.my.model.LecturerCourse;
import java.util.List;

/**
 *
 * @author Afsara Tasnim Mim
 */
public interface LecturerCourseInterface {
    
    public List<LecturerCourse> getLecturerCourse();

    public void createLecturerCourse(LecturerCourse lecturerCourse);

    public void updateLecturerCourse(LecturerCourse lecturerCourse);

    public void deleteLecturerCourse(LecturerCourse lecturerCourse);
}
