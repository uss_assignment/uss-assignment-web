/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.Interface;

import bd.ac.seu.my.model.Programme;
import java.util.List;

/**
 *
 * @author Afsara Tasnim Mim
 */
public interface ProrammeInterface {
    
    public List<Programme> getProramme();

    public void createProramme(Programme programme);

    public void updateProramme(Programme programme);

    public void deleteProramme(Programme programme);
}
