/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.Interface;

import bd.ac.seu.my.model.Sport;
import java.util.List;

/**
 *
 * @author Afsara Tasnim Mim
 */
public interface SportsInterface {
    
    public List<Sport> getSports();

    public void createSports(Sport sport);

    public void updateSports(Sport sport);

    public void deleteSports(Sport sport);
}
