/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.Interface;

import bd.ac.seu.my.model.PreCourse;
import java.util.List;

/**
 *
 * @author Afsara Tasnim Mim
 */
public interface PreCourseInterface {
    
    public List<PreCourse> getPreCourse();

    public void createPreCourse(PreCourse preCourse);

    public void updatePreCourse(PreCourse preCourse);

    public void deletePreCourse(PreCourse preCourse);
}
