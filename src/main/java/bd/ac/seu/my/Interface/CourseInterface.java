/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.Interface;

import bd.ac.seu.my.model.Course;
import java.util.List;

/**
 *
 * @author Afsara Tasnim Mim
 */
public interface CourseInterface {
    
    public List<Course> getCourse();

    public void createCourse(Course course);

    public void updateCourse(Course course);

    public void deleteCourse(Course course);
}
