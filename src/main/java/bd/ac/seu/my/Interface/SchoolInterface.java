/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.Interface;

import bd.ac.seu.my.model.School;
import java.util.List;

/**
 *
 * @author Afsara Tasnim Mim
 */
public interface SchoolInterface {
    
    public List<School> getSchools();

    public void createSchool(School school);

    public void updateSchool(School school);

    public void deleteSchool(School school);
}
