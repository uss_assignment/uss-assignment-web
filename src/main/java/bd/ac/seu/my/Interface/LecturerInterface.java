/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.Interface;

import bd.ac.seu.my.model.Lecturer;
import java.util.List;

/**
 *
 * @author Afsara Tasnim Mim
 */
public interface LecturerInterface {
    
    public List<Lecturer> getLecturers();

    public void createLecturer(Lecturer lecturer);

    public void updateLecturer(Lecturer lecturer);

    public void deleteLecturer(Lecturer lecturer);
}
