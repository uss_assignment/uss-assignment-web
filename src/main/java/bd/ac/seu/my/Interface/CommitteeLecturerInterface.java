package bd.ac.seu.my.Interface;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import bd.ac.seu.my.model.CommitteeLecturer;
import java.util.List;

/**
 *
 * @author Afsara Tasnim Mim
 */
public interface CommitteeLecturerInterface{
    
    public List<CommitteeLecturer> getCommittiLecturer();

    public void createCommittiLecturer(CommitteeLecturer committiLecturer);

    public void updateCommittiLecturer(CommitteeLecturer committiLecturer);

    public void deleteCommittiLecturer(CommitteeLecturer committiLecturer);
}
