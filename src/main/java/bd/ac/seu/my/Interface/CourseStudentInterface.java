/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.Interface;

import bd.ac.seu.my.model.CourseStudent;
import java.util.List;

/**
 *
 * @author Afsara Tasnim Mim
 */
public interface CourseStudentInterface {
    
    public List<CourseStudent> getCourseStudent();

    public void createCourseStudent(CourseStudent courseStudent);

    public void updateCourseStudent(CourseStudent courseStudent);

    public void deleteCourseStudent(CourseStudent courseStudent);
}
