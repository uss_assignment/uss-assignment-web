/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.Interface;

import bd.ac.seu.my.model.Campus;

import java.util.List;

/**
 *
 * @author Afsara Tasnim Mim
 */
public interface CampusInterface {

    public List<Campus> getCampuses();

    public void createCampus(Campus campus);

    public void updateCampus(Campus campus);

    public void deleteCampus(Campus campus);
}
