/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CourseDAO;
import bd.ac.seu.my.DAO.CourseStudentDAO;
import bd.ac.seu.my.DAO.CourseStudentDAO;
import bd.ac.seu.my.DAO.StudentDAO;
import bd.ac.seu.my.model.Course;
import bd.ac.seu.my.model.CourseStudent;
import bd.ac.seu.my.model.LecturerCourse;
import bd.ac.seu.my.model.Student;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Asaduzzaman Noor
 */
@ManagedBean
@SessionScoped
public class StudentCourseController {
    private CourseStudent courseStudent;
    private CourseStudent selectedCourseStudent;
    private List<CourseStudent> courseStudentList;
    private List<CourseStudent> filteredCourseStudent;
    private List<Course> courseList;
    private List<Student> studentList;

    public StudentCourseController() {
        courseStudent = new CourseStudent();
        CourseStudentDAO courseStudentDAO = new CourseStudentDAO();
        courseStudentList = courseStudentDAO.getCourseStudent();
        CourseDAO courseDAO = new CourseDAO();
        courseList = courseDAO.getCourse();
        StudentDAO studentDAO = new StudentDAO();
        studentList = studentDAO.getStudent();
        filteredCourseStudent = new ArrayList<>();
    }

    public CourseStudent getCourseStudent() {
        return courseStudent;
    }

    public void setCourseStudent(CourseStudent courseStudent) {
        this.courseStudent = courseStudent;
    }

    public CourseStudent getSelectedCourseStudent() {
        return selectedCourseStudent;
    }

    public void setSelectedCourseStudent(CourseStudent selectedCourseStudent) {
        this.selectedCourseStudent = selectedCourseStudent;
    }

    public List<CourseStudent> getCourseStudentList() {
        return courseStudentList;
    }

    public void setCourseStudentList(List<CourseStudent> courseStudentList) {
        this.courseStudentList = courseStudentList;
    }

    public List<CourseStudent> getFilteredCourseStudent() {
        return filteredCourseStudent;
    }

    public void setFilteredCourseStudent(List<CourseStudent> filteredCourseStudent) {
        this.filteredCourseStudent = filteredCourseStudent;
    }

    public List<Course> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<Course> courseList) {
        this.courseList = courseList;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    
    public void addEvent() {

        CourseStudentDAO courseStudentDAO = new CourseStudentDAO();
        courseStudentDAO.createCourseStudent(courseStudent);
        courseStudentList.clear();
        courseStudentList = courseStudentDAO.getCourseStudent();

        FacesMessage msg = new FacesMessage("CourseStudent Added");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowEdit(RowEditEvent event) {
        CourseStudentDAO courseStudentDAO = new CourseStudentDAO();
        courseStudentDAO.updateCourseStudent((CourseStudent) event.getObject());

        FacesMessage msg = new FacesMessage("LecturerCourse Edited", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("CourseStudent Edit Cancelled", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowDelete() {
        CourseStudentDAO courseStudentDAO = new CourseStudentDAO();
        courseStudentDAO.deleteCourseStudent(selectedCourseStudent);
        courseStudentList.clear();
        courseStudentList = courseStudentDAO.getCourseStudent();

        FacesMessage msg = new FacesMessage("CourseStudent Deleted", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}
