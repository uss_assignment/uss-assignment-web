/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CampusDAO;
import bd.ac.seu.my.model.Campus;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Asaduzzaman Noor
 */
@ManagedBean
@SessionScoped
public class CampusController implements Serializable {

    private Campus campus;
    private Campus selectedCampus;
    private List<Campus> campusList;
    private List<Campus> filteredCampus;

    public CampusController() {
        campus = new Campus();
        CampusDAO campusDAO = new CampusDAO();
        campusList = campusDAO.getCampuses();
        filteredCampus = new ArrayList<>();
    }

    public Campus getCampus() {
        return campus;
    }

    public void setCampus(Campus campus) {
        this.campus = campus;
    }

    public List<Campus> getCampusList() {
        return campusList;
    }

    public List<Campus> getFilteredCampus() {
        return filteredCampus;
    }

    public void setFilteredCampus(List<Campus> filteredCampus) {
        this.filteredCampus = filteredCampus;
    }
    
    public void setCampusList(List<Campus> campusList) {
        this.campusList = campusList;
    }

    public Campus getSelectedCampus() {
        return selectedCampus;
    }

    public void setSelectedCampus(Campus selectedCampus) {
        this.selectedCampus = selectedCampus;
    }
    
    
    public void addCampus() {
        /*
        if (campusList.stream().anyMatch(c1 -> c1.getCampusName().equals(campus.getCampusName()))) {
            FacesMessage msg = new FacesMessage("Campus Name Already Exists");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }else{
*/
            CampusDAO campusDAO = new CampusDAO();
            campusDAO.createCampus(campus);
            campusList.clear();
            campusList = campusDAO.getCampuses();

            FacesMessage msg = new FacesMessage("Campus Added");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        //}
        
    }
    
    public void onRowEdit(RowEditEvent event) {
        CampusDAO campusDAO = new CampusDAO();
        campusDAO.updateCampus((Campus) event.getObject());
        
        FacesMessage msg = new FacesMessage("Campus Edited", ((Campus) event.getObject()).getCampusName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
     
    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Campus Edit Cancelled", ((Campus) event.getObject()).getCampusName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
     
    public void onRowDelete() {
        CampusDAO campusDAO = new CampusDAO();
        campusDAO.deleteCampus(selectedCampus);
        campusList.clear();
        campusList = campusDAO.getCampuses();
        
        FacesMessage msg = new FacesMessage("Campus Deleted", selectedCampus.getCampusName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    
}
