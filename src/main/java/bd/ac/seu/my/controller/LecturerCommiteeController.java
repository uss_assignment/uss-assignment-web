/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CommitteeDAO;
import bd.ac.seu.my.DAO.CommitteeLecturerDAO;
import bd.ac.seu.my.DAO.FacultyDAO;
import bd.ac.seu.my.DAO.LecturerDAO;
import bd.ac.seu.my.model.Committee;
import bd.ac.seu.my.model.CommitteeLecturer;
import bd.ac.seu.my.model.Faculty;
import bd.ac.seu.my.model.Lecturer;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Asaduzzaman Noor
 */
@ManagedBean
@SessionScoped
public class LecturerCommiteeController {
    private CommitteeLecturer committeeLecturer;
    private CommitteeLecturer selectedCommitteeLecturer;
    private List<CommitteeLecturer> committeeLecturerList;
    private List<CommitteeLecturer> filteredCommitteeLecturers;
    private List<Lecturer> lecturerList;
    private List<Committee> committeeList;
    private List<Faculty> facultyList;

    public LecturerCommiteeController() {
        committeeLecturer = new CommitteeLecturer();
        CommitteeLecturerDAO committeeLecturerDAO = new CommitteeLecturerDAO();
        committeeLecturerList = committeeLecturerDAO.getCommittiLecturer();
        LecturerDAO lecturerDAO = new LecturerDAO();
        lecturerList = lecturerDAO.getLecturers();
        CommitteeDAO committeeDAO = new CommitteeDAO();
        committeeList = committeeDAO.getCommittee();
        FacultyDAO facultyDAO = new FacultyDAO();
        facultyList = facultyDAO.getFaculties();
        filteredCommitteeLecturers = new ArrayList<>();
    }

    public CommitteeLecturer getCommitteeLecturer() {
        return committeeLecturer;
    }

    public void setCommitteeLecturer(CommitteeLecturer committeeLecturer) {
        this.committeeLecturer = committeeLecturer;
    }

    public CommitteeLecturer getSelectedCommitteeLecturer() {
        return selectedCommitteeLecturer;
    }

    public void setSelectedCommitteeLecturer(CommitteeLecturer selectedCommitteeLecturer) {
        this.selectedCommitteeLecturer = selectedCommitteeLecturer;
    }

    public List<CommitteeLecturer> getSelectedCommitteeLecturerList() {
        return committeeLecturerList;
    }

    public void setSelectedCommitteeLecturerList(List<CommitteeLecturer> committeeLecturerList) {
        this.committeeLecturerList = committeeLecturerList;
    }

    public List<CommitteeLecturer> getFilteredCommitteeLecturers() {
        return filteredCommitteeLecturers;
    }

    public void setFilteredCommitteeLecturers(List<CommitteeLecturer> filteredCommitteeLecturers) {
        this.filteredCommitteeLecturers = filteredCommitteeLecturers;
    }

    public List<Lecturer> getLecturerList() {
        return lecturerList;
    }

    public void setLecturerList(List<Lecturer> lecturerList) {
        this.lecturerList = lecturerList;
    }

    public List<Committee> getCommitteeList() {
        return committeeList;
    }

    public void setCommitteeList(List<Committee> committeeList) {
        this.committeeList = committeeList;
    }

    public List<Faculty> getFacultyList() {
        return facultyList;
    }

    public void setFacultyList(List<Faculty> facultyList) {
        this.facultyList = facultyList;
    }

    public List<CommitteeLecturer> getCommitteeLecturerList() {
        return committeeLecturerList;
    }

    public void setCommitteeLecturerList(List<CommitteeLecturer> committeeLecturerList) {
        this.committeeLecturerList = committeeLecturerList;
    }
    
    
    public void addCommitteeLecturer() {

        CommitteeLecturerDAO committeeLecturerDAO = new CommitteeLecturerDAO();
        committeeLecturerDAO.createCommittiLecturer(committeeLecturer);
        committeeLecturerList.clear();
        committeeLecturerList = committeeLecturerDAO.getCommittiLecturer();

        FacesMessage msg = new FacesMessage("Committee Lecturer Added");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowEdit(RowEditEvent event) {
        CommitteeLecturerDAO committeeLecturerDAO = new CommitteeLecturerDAO();
        committeeLecturerDAO.updateCommittiLecturer((CommitteeLecturer) event.getObject());

        FacesMessage msg = new FacesMessage("Committee Edited", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Committee Edit Cancelled", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowDelete() {
        CommitteeLecturerDAO committeeLecturerDAO = new CommitteeLecturerDAO();
        committeeLecturerDAO.deleteCommittiLecturer(selectedCommitteeLecturer);
        committeeLecturerList.clear();
        committeeLecturerList = committeeLecturerDAO.getCommittiLecturer();

        FacesMessage msg = new FacesMessage("Committee Deleted", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
}
