/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.ClubDAO;
import bd.ac.seu.my.DAO.SportsDAO;
import bd.ac.seu.my.model.Club;
import bd.ac.seu.my.model.Sport;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Asaduzzaman Noor
 */
@ManagedBean
@SessionScoped
public class SportsController {
    private Sport sport;
    private Sport selectedSport;
    private List<Sport> sportList;
    private List<Sport> filteredSports;
    private List<Club> clubList;

    public SportsController() {
        sport = new Sport();
        SportsDAO sportsDAO = new SportsDAO();
        sportList = sportsDAO.getSports();
        ClubDAO clubDAO = new ClubDAO();
        clubList = clubDAO.getClub();
        filteredSports = new ArrayList<>();
    }

    public Sport getSport() {
        return sport;
    }

    public void setSport(Sport sport) {
        this.sport = sport;
    }

    public Sport getSelectedSport() {
        return selectedSport;
    }

    public void setSelectedSport(Sport selectedSport) {
        this.selectedSport = selectedSport;
    }

    public List<Sport> getSportList() {
        return sportList;
    }

    public void setSportList(List<Sport> sportList) {
        this.sportList = sportList;
    }

    public List<Sport> getFilteredSports() {
        return filteredSports;
    }

    public void setFilteredSports(List<Sport> filteredSports) {
        this.filteredSports = filteredSports;
    }

    public List<Club> getClubList() {
        return clubList;
    }

    public void setClubList(List<Club> clubList) {
        this.clubList = clubList;
    }
    
    
    public void addSports() {

        SportsDAO sportsDAO = new SportsDAO();
        sportsDAO.createSports(sport);
        sportList.clear();
        sportList = sportsDAO.getSports();

        FacesMessage msg = new FacesMessage("Sports Added");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowEdit(RowEditEvent event) {
        SportsDAO sportsDAO = new SportsDAO();
        sportsDAO.updateSports((Sport) event.getObject());

        FacesMessage msg = new FacesMessage("Sports Edited", ((Sport) event.getObject()).getSportName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Sports Edit Cancelled", ((Sport) event.getObject()).getSportName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowDelete() {
        SportsDAO sportsDAO = new SportsDAO();
        sportsDAO.deleteSports(selectedSport);
        sportList.clear();
        sportList = sportsDAO.getSports();

        FacesMessage msg = new FacesMessage("Sports Deleted", selectedSport.getSportName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

}
