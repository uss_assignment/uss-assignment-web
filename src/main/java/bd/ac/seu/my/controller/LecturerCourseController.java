/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CommitteeDAO;
import bd.ac.seu.my.DAO.LecturerCourseDAO;
import bd.ac.seu.my.DAO.CourseDAO;
import bd.ac.seu.my.DAO.FacultyDAO;
import bd.ac.seu.my.DAO.LecturerCourseDAO;
import bd.ac.seu.my.DAO.LecturerDAO;
import bd.ac.seu.my.model.Committee;
import bd.ac.seu.my.model.CommitteeLecturer;
import bd.ac.seu.my.model.Course;
import bd.ac.seu.my.model.LecturerCourse;
import bd.ac.seu.my.model.Faculty;
import bd.ac.seu.my.model.Lecturer;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Asaduzzaman Noor
 */
@ManagedBean
@SessionScoped
public class LecturerCourseController {
    private LecturerCourse lecturerCourse;
    private LecturerCourse selectedLecturerCourse;
    private List<LecturerCourse> lecturerCourseList;
    private List<LecturerCourse> filteredLecturerCourses;
    private List<Lecturer> lecturerList;
    private List<Course> courseList;

    public LecturerCourseController() {
        lecturerCourse = new LecturerCourse();
        LecturerCourseDAO lecturerCourseDAO = new LecturerCourseDAO();
        lecturerCourseList = lecturerCourseDAO.getLecturerCourse();
        LecturerDAO lecturerDAO = new LecturerDAO();
        lecturerList = lecturerDAO.getLecturers();
        CourseDAO courseDAO = new CourseDAO();
        courseList = courseDAO.getCourse();
        filteredLecturerCourses = new ArrayList<>();
    }

    public LecturerCourse getLecturerCourse() {
        return lecturerCourse;
    }

    public void setLecturerCourse(LecturerCourse lecturerCourse) {
        this.lecturerCourse = lecturerCourse;
    }

    public LecturerCourse getSelectedLecturerCourse() {
        return selectedLecturerCourse;
    }

    public void setSelectedLecturerCourse(LecturerCourse selectedLecturerCourse) {
        this.selectedLecturerCourse = selectedLecturerCourse;
    }

    public List<LecturerCourse> getLecturerCourseList() {
        return lecturerCourseList;
    }

    public void setLecturerCourseList(List<LecturerCourse> lecturerCourseList) {
        this.lecturerCourseList = lecturerCourseList;
    }

    public List<LecturerCourse> getFilteredLecturerCourses() {
        return filteredLecturerCourses;
    }

    public void setFilteredLecturerCourses(List<LecturerCourse> filteredLecturerCourses) {
        this.filteredLecturerCourses = filteredLecturerCourses;
    }

    public List<Lecturer> getLecturerList() {
        return lecturerList;
    }

    public void setLecturerList(List<Lecturer> lecturerList) {
        this.lecturerList = lecturerList;
    }

    public List<Course> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<Course> courseList) {
        this.courseList = courseList;
    }

    
    
    public void addEvent() {

        LecturerCourseDAO lecturerCourseDAO = new LecturerCourseDAO();
        lecturerCourseDAO.createLecturerCourse(lecturerCourse);
        lecturerCourseList.clear();
        lecturerCourseList = lecturerCourseDAO.getLecturerCourse();

        FacesMessage msg = new FacesMessage("LecturerCourse Added");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowEdit(RowEditEvent event) {
        LecturerCourseDAO lecturerCourseDAO = new LecturerCourseDAO();
        lecturerCourseDAO.updateLecturerCourse((LecturerCourse) event.getObject());

        FacesMessage msg = new FacesMessage("LecturerCourse Edited", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("LecturerCourse Edit Cancelled", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowDelete() {
        LecturerCourseDAO lecturerCourseDAO = new LecturerCourseDAO();
        lecturerCourseDAO.deleteLecturerCourse(selectedLecturerCourse);
        lecturerCourseList.clear();
        lecturerCourseList = lecturerCourseDAO.getLecturerCourse();

        FacesMessage msg = new FacesMessage("LecturerCourse Deleted", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}
