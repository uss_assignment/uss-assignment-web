/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CourseDAO;
import bd.ac.seu.my.DAO.PreCourseDAO;
import bd.ac.seu.my.DAO.ProrammeDAO;
import bd.ac.seu.my.model.Course;
import bd.ac.seu.my.model.PreCourse;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Asaduzzaman Noor
 */
@ManagedBean
@SessionScoped
public class PreCourseController {
    
    private PreCourse preCourse;
    private PreCourse selectedPreCourse;
    private List<PreCourse> preCourseList;
    private List<Course> courseList;
    private List<PreCourse> filteredPreCourse;

    public PreCourseController() {
        preCourse = new PreCourse();
        PreCourseDAO preCourseDAO = new PreCourseDAO();
        preCourseList = preCourseDAO.getPreCourse();
        CourseDAO courseDAO = new CourseDAO();
        courseList = courseDAO.getCourse();
        filteredPreCourse = new ArrayList<>();
    }
    
    public void addPreCourse() {

        PreCourseDAO preCourseDAO = new PreCourseDAO();
        preCourseDAO.createPreCourse(preCourse);
        preCourseList.clear();
        preCourseList = preCourseDAO.getPreCourse();

        FacesMessage msg = new FacesMessage("Pre Course Added");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowEdit(RowEditEvent event) {
        PreCourseDAO preCourseDAO = new PreCourseDAO();
        preCourseDAO.updatePreCourse((PreCourse) event.getObject());

        FacesMessage msg = new FacesMessage("Pre Course Edited", ((PreCourse) event.getObject()).getCourse().getCourseCode());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Pre Course Edit Cancelled", ((PreCourse) event.getObject()).getCourse().getCourseCode());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowDelete() {
        PreCourseDAO preCourseDAO = new PreCourseDAO();
        preCourseDAO.deletePreCourse(selectedPreCourse);
        preCourseList.clear();
        preCourseList = preCourseDAO.getPreCourse();

        FacesMessage msg = new FacesMessage("Pre Course Deleted", selectedPreCourse.getCourse().getCourseCode());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public PreCourse getPreCourse() {
        return preCourse;
    }

    public void setPreCourse(PreCourse preCourse) {
        this.preCourse = preCourse;
    }

    public PreCourse getSelectedPreCourse() {
        return selectedPreCourse;
    }

    public void setSelectedPreCourse(PreCourse selectedPreCourse) {
        this.selectedPreCourse = selectedPreCourse;
    }

    public List<PreCourse> getPreCourseList() {
        return preCourseList;
    }

    public void setPreCourseList(List<PreCourse> preCourseList) {
        this.preCourseList = preCourseList;
    }

    public List<Course> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<Course> courseList) {
        this.courseList = courseList;
    }

    public List<PreCourse> getFilteredPreCourse() {
        return filteredPreCourse;
    }

    public void setFilteredPreCourse(List<PreCourse> filteredPreCourse) {
        this.filteredPreCourse = filteredPreCourse;
    }
}
