/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.FacultyDAO;
import bd.ac.seu.my.model.Faculty;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Asaduzzaman Noor
 */
@ManagedBean
@SessionScoped
public class FacultyController  implements Serializable {

    private Faculty faculty;
    private Faculty selectedFaculty;
    private List<Faculty> facultyList;
    private List<Faculty> filteredFaculty;

    public FacultyController() {
        faculty = new Faculty();
        FacultyDAO facultyDAO = new FacultyDAO();
        facultyList = facultyDAO.getFaculties();
        filteredFaculty = new ArrayList<>();
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public List<Faculty> getFacultyList() {
        return facultyList;
    }

    public List<Faculty> getFilteredFaculty() {
        return filteredFaculty;
    }

    public void setFilteredFaculty(List<Faculty> filteredFaculty) {
        this.filteredFaculty = filteredFaculty;
    }
    
    public void setFacultyList(List<Faculty> facultyList) {
        this.facultyList = facultyList;
    }

    public Faculty getSelectedFaculty() {
        return selectedFaculty;
    }

    public void setSelectedFaculty(Faculty selectedFaculty) {
        this.selectedFaculty = selectedFaculty;
    }
    
    
    public void addFaculty() {
        /*
        if (facultyList.stream().anyMatch(c1 -> c1.getFacultyName().equals(faculty.getFacultyName()))) {
            FacesMessage msg = new FacesMessage("Faculty Name Already Exists");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }else{
*/
            FacultyDAO facultyDAO = new FacultyDAO();
            facultyDAO.createFaculty(faculty);
            facultyList.clear();
            facultyList = facultyDAO.getFaculties();

            FacesMessage msg = new FacesMessage("Faculty Added");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        //}
        
    }
    
    public void onRowEdit(RowEditEvent event) {
        FacultyDAO facultyDAO = new FacultyDAO();
        facultyDAO.updateFaculty((Faculty) event.getObject());
        
        FacesMessage msg = new FacesMessage("Faculty Edited", ((Faculty) event.getObject()).getFacultyName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
     
    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Faculty Edit Cancelled", ((Faculty) event.getObject()).getFacultyName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
     
    public void onRowDelete() {
        FacultyDAO facultyDAO = new FacultyDAO();
        facultyDAO.deleteFaculty(selectedFaculty);
        facultyList.clear();
        facultyList = facultyDAO.getFaculties();
        
        FacesMessage msg = new FacesMessage("Faculty Deleted", selectedFaculty.getFacultyName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    
}
