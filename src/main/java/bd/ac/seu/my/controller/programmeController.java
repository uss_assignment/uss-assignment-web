/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CommitteeDAO;
import bd.ac.seu.my.DAO.ProrammeDAO;
import bd.ac.seu.my.DAO.SchoolDAO;
import bd.ac.seu.my.model.Committee;
import bd.ac.seu.my.model.Programme;
import bd.ac.seu.my.model.School;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Asaduzzaman Noor
 */
@ManagedBean
@SessionScoped
public class programmeController {
    private Programme programme;
    private Programme selectedProgramme;
    private List<Programme> programmeList;
    private List<Programme> filteredProgramme;
    private List<School> schoolList;

    public programmeController() {
        programme = new Programme();
        ProrammeDAO prorammeDAO = new ProrammeDAO();
        programmeList = prorammeDAO.getProramme();
        SchoolDAO schoolDAO = new SchoolDAO();
        schoolList = schoolDAO.getSchools();
        filteredProgramme = new ArrayList<>();
    }

    public Programme getProgramme() {
        return programme;
    }

    public void setProgramme(Programme programme) {
        this.programme = programme;
    }

    public Programme getSelectedProgramme() {
        return selectedProgramme;
    }

    public void setSelectedProgramme(Programme selectedProgramme) {
        this.selectedProgramme = selectedProgramme;
    }

    public List<Programme> getProgrammeList() {
        return programmeList;
    }

    public void setProgrammeList(List<Programme> programmeList) {
        this.programmeList = programmeList;
    }

    public List<Programme> getFilteredProgramme() {
        return filteredProgramme;
    }

    public void setFilteredProgramme(List<Programme> filteredProgramme) {
        this.filteredProgramme = filteredProgramme;
    }

    public List<School> getSchoolList() {
        return schoolList;
    }

    public void setSchoolList(List<School> schoolList) {
        this.schoolList = schoolList;
    }
    
    public void addProgramme() {

        ProrammeDAO prorammeDAO = new ProrammeDAO();
        prorammeDAO.createProramme(programme);
        programmeList.clear();
        programmeList = prorammeDAO.getProramme();

        FacesMessage msg = new FacesMessage("Program Added");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowEdit(RowEditEvent event) {
        ProrammeDAO prorammeDAO = new ProrammeDAO();
        prorammeDAO.updateProramme((Programme) event.getObject());

        FacesMessage msg = new FacesMessage("Program Edited", ((Programme) event.getObject()).getProgrammeCode());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Program Edit Cancelled", ((Programme) event.getObject()).getProgrammeCode());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowDelete() {
        ProrammeDAO prorammeDAO = new ProrammeDAO();
        prorammeDAO.deleteProramme(selectedProgramme);
        programmeList.clear();
        programmeList = prorammeDAO.getProramme();

        FacesMessage msg = new FacesMessage("Program Deleted", selectedProgramme.getProgrammeCode());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}
