/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CourseDAO;
import bd.ac.seu.my.DAO.ProrammeDAO;
import bd.ac.seu.my.DAO.StudentDAO;
import bd.ac.seu.my.model.Course;
import bd.ac.seu.my.model.Student;
import bd.ac.seu.my.model.Programme;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Asaduzzaman Noor
 */
@ManagedBean
@SessionScoped
public class StudentController {
    
    private Student student;
    private Student selectedStudent;
    private List<Student> studentList;
    private List<Student> filteredStudent;
    private List<Programme> programmeList;

    public StudentController() {
        student = new Student();
        StudentDAO studentDAO = new StudentDAO();
        studentList = studentDAO.getStudent();
        ProrammeDAO prorammeDAO = new ProrammeDAO();
        programmeList = prorammeDAO.getProramme();
        filteredStudent = new ArrayList<>();
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Student getSelectedStudent() {
        return selectedStudent;
    }

    public void setSelectedStudent(Student selectedStudent) {
        this.selectedStudent = selectedStudent;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    public List<Student> getFilteredStudent() {
        return filteredStudent;
    }

    public void setFilteredStudent(List<Student> filteredStudent) {
        this.filteredStudent = filteredStudent;
    }

    public List<Programme> getProgrammeList() {
        return programmeList;
    }

    public void setProgrammeList(List<Programme> programmeList) {
        this.programmeList = programmeList;
    }

    
    public void addStudent() {

        StudentDAO studentDAO = new StudentDAO();
        studentDAO.createStudent(student);
        studentList.clear();
        studentList = studentDAO.getStudent();

        FacesMessage msg = new FacesMessage("Student Added");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowEdit(RowEditEvent event) {
        StudentDAO studentDAO = new StudentDAO();
        studentDAO.updateStudent((Student) event.getObject());

        FacesMessage msg = new FacesMessage("Student Edited", ((Student) event.getObject()).getStudentName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Student Edit Cancelled", ((Student) event.getObject()).getStudentName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowDelete() {
        StudentDAO studentDAO = new StudentDAO();
        studentDAO.deleteStudent(selectedStudent);
        studentList.clear();
        studentList = studentDAO.getStudent();

        FacesMessage msg = new FacesMessage("Student Deleted", selectedStudent.getStudentName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}