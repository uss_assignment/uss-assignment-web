/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.ClubDAO;
import bd.ac.seu.my.DAO.CampusDAO;
import bd.ac.seu.my.model.Club;
import bd.ac.seu.my.model.Campus;
import bd.ac.seu.my.model.Committee;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Asaduzzaman Noor
 */
@ManagedBean
@SessionScoped
public class ClubController {
    private Club club;
    private Club selectedClub;
    private List<Club> clubList;
    private List<Club> filteredClubs;
    private List<Campus> campusList;

    public ClubController() {
        club = new Club();
        ClubDAO clubDAO = new ClubDAO();
        clubList = clubDAO.getClub();
        CampusDAO campusDAO = new CampusDAO();
        campusList = campusDAO.getCampuses();
        filteredClubs = new ArrayList<>();
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public Club getSelectedClub() {
        return selectedClub;
    }

    public void setSelectedClub(Club selectedClub) {
        this.selectedClub = selectedClub;
    }

    public List<Club> getClubList() {
        return clubList;
    }

    public void setClubList(List<Club> clubList) {
        this.clubList = clubList;
    }

    public List<Club> getFilteredClubs() {
        return filteredClubs;
    }

    public void setFilteredClubs(List<Club> filteredClubs) {
        this.filteredClubs = filteredClubs;
    }

    public List<Campus> getCampusList() {
        return campusList;
    }

    public void setCampusList(List<Campus> campusList) {
        this.campusList = campusList;
    }
    
    public void addClub() {

        ClubDAO clubDAO = new ClubDAO();
        clubDAO.createClub(club);
        clubList.clear();
        clubList = clubDAO.getClub();

        FacesMessage msg = new FacesMessage("Club Added");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowEdit(RowEditEvent event) {
        ClubDAO clubDAO = new ClubDAO();
        clubDAO.updateClub((Club) event.getObject());

        FacesMessage msg = new FacesMessage("Club Edited", ((Club) event.getObject()).getClubName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Club Edit Cancelled", ((Club) event.getObject()).getClubName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowDelete() {
        ClubDAO clubDAO = new ClubDAO();
        clubDAO.deleteClub(selectedClub);
        clubList.clear();
        clubList = clubDAO.getClub();

        FacesMessage msg = new FacesMessage("Club Deleted", selectedClub.getClubName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

}
