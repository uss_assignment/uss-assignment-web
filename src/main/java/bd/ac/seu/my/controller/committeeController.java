/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CampusDAO;
import bd.ac.seu.my.DAO.CommitteeDAO;
import bd.ac.seu.my.DAO.FacultyDAO;
import bd.ac.seu.my.DAO.SchoolDAO;
import bd.ac.seu.my.model.Campus;
import bd.ac.seu.my.model.Committee;
import bd.ac.seu.my.model.Faculty;
import bd.ac.seu.my.model.School;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Asaduzzaman Noor
 */
@ManagedBean
@SessionScoped
public class committeeController {
    private Committee committee;
    private Committee selectedCommittee;
    private List<Committee> committeeList;
    private List<Committee> filteredCommittees;
    private List<Faculty> facultyList;

    public committeeController() {
        committee = new Committee();
        CommitteeDAO committeeDAO = new CommitteeDAO();
        committeeList = committeeDAO.getCommittee();
        FacultyDAO facultyDAO = new FacultyDAO();
        facultyList = facultyDAO.getFaculties();
        filteredCommittees = new ArrayList<>();
    }

    public Committee getCommittee() {
        return committee;
    }

    public void setCommittee(Committee committee) {
        this.committee = committee;
    }

    public Committee getSelectedCommittee() {
        return selectedCommittee;
    }

    public void setSelectedCommittee(Committee selectedCommittee) {
        this.selectedCommittee = selectedCommittee;
    }

    public List<Committee> getCommitteeList() {
        return committeeList;
    }

    public void setCommitteeList(List<Committee> committeeList) {
        this.committeeList = committeeList;
    }

    public List<Committee> getFilteredCommittees() {
        return filteredCommittees;
    }

    public void setFilteredCommittees(List<Committee> filteredCommittees) {
        this.filteredCommittees = filteredCommittees;
    }

    public List<Faculty> getFacultyList() {
        return facultyList;
    }

    public void setFacultyList(List<Faculty> facultyList) {
        this.facultyList = facultyList;
    }
    
    
    public void addCommittee() {

        CommitteeDAO committeeDAO = new CommitteeDAO();
        committeeDAO.createCommittee(committee);
        committeeList.clear();
        committeeList = committeeDAO.getCommittee();

        FacesMessage msg = new FacesMessage("Committee Added");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowEdit(RowEditEvent event) {
        CommitteeDAO committeeDAO = new CommitteeDAO();
        committeeDAO.updateCommittee((Committee) event.getObject());

        FacesMessage msg = new FacesMessage("Committee Edited", ((Committee) event.getObject()).getCommitteeTitle());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Committee Edit Cancelled", ((Committee) event.getObject()).getCommitteeTitle());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowDelete() {
        CommitteeDAO committeeDAO = new CommitteeDAO();
        committeeDAO.deleteCommittee(selectedCommittee);
        committeeList.clear();
        committeeList = committeeDAO.getCommittee();

        FacesMessage msg = new FacesMessage("Committee Deleted", selectedCommittee.getCommitteeTitle());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

}
