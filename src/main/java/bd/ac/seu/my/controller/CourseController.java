/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CourseDAO;
import bd.ac.seu.my.DAO.ProrammeDAO;
import bd.ac.seu.my.DAO.SchoolDAO;
import bd.ac.seu.my.model.Course;
import bd.ac.seu.my.model.Programme;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Asaduzzaman Noor
 */
@ManagedBean
@SessionScoped
public class CourseController {
    
    private Course course;
    private Course selectedCourse;
    private List<Course> courseList;
    private List<Course> filteredCourse;
    private List<Programme> programmeList;

    public CourseController() {
        course = new Course();
        CourseDAO courseDAO = new CourseDAO();
        courseList = courseDAO.getCourse();
        ProrammeDAO prorammeDAO = new ProrammeDAO();
        programmeList = prorammeDAO.getProramme();
        filteredCourse = new ArrayList<>();
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Course getSelectedCourse() {
        return selectedCourse;
    }

    public void setSelectedCourse(Course selectedCourse) {
        this.selectedCourse = selectedCourse;
    }

    public List<Course> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<Course> courseList) {
        this.courseList = courseList;
    }

    public List<Course> getFilteredCourse() {
        return filteredCourse;
    }

    public void setFilteredCourse(List<Course> filteredCourse) {
        this.filteredCourse = filteredCourse;
    }

    public List<Programme> getProgrammeList() {
        return programmeList;
    }

    public void setProgrammeList(List<Programme> programmeList) {
        this.programmeList = programmeList;
    }
    
    
    public void addCourse() {

        CourseDAO courseDAO = new CourseDAO();
        courseDAO.createCourse(course);
        courseList.clear();
        courseList = courseDAO.getCourse();

        FacesMessage msg = new FacesMessage("Course Added");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowEdit(RowEditEvent event) {
        CourseDAO courseDAO = new CourseDAO();
        courseDAO.updateCourse((Course) event.getObject());

        FacesMessage msg = new FacesMessage("Course Edited", ((Course) event.getObject()).getCourseCode());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Course Edit Cancelled", ((Course) event.getObject()).getCourseCode());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowDelete() {
        CourseDAO courseDAO = new CourseDAO();
        courseDAO.deleteCourse(selectedCourse);
        courseList.clear();
        courseList = courseDAO.getCourse();

        FacesMessage msg = new FacesMessage("Course Deleted", selectedCourse.getCourseCode());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}