/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CampusDAO;
import bd.ac.seu.my.DAO.FacultyDAO;
import bd.ac.seu.my.DAO.LecturerDAO;
import bd.ac.seu.my.DAO.SchoolDAO;
import bd.ac.seu.my.model.Campus;
import bd.ac.seu.my.model.Faculty;
import bd.ac.seu.my.model.Lecturer;
import bd.ac.seu.my.model.School;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Asaduzzaman Noor
 */

@ManagedBean
@SessionScoped
public class LecturerController implements Serializable {

    private Lecturer lecturer;
    private Lecturer selectedLecturer;
    private List<School> schoolList;
    private List<Lecturer> lecturerList;
    private List<Lecturer> filteredLecturers;
    
    public LecturerController() {
        lecturer = new Lecturer();
        LecturerDAO lecturerDAO = new LecturerDAO();
        lecturerList = lecturerDAO.getLecturers();
        
        SchoolDAO schoolDAO = new SchoolDAO();
        schoolList = schoolDAO.getSchools();
        filteredLecturers = new ArrayList<>();
    }

    public Lecturer getLecturer() {
        return lecturer;
    }

    public void setLecturer(Lecturer lecturer) {
        this.lecturer = lecturer;
    }

    public Lecturer getSelectedLecturer() {
        return selectedLecturer;
    }

    public void setSelectedLecturer(Lecturer selectedLecturer) {
        this.selectedLecturer = selectedLecturer;
    }

    public List<School> getSchoolList() {
        return schoolList;
    }

    public void setSchoolList(List<School> schoolList) {
        this.schoolList = schoolList;
    }

    public List<Lecturer> getLecturerList() {
        return lecturerList;
    }

    public void setLecturerList(List<Lecturer> lecturerList) {
        this.lecturerList = lecturerList;
    }

    public List<Lecturer> getFilteredLecturers() {
        return filteredLecturers;
    }

    public void setFilteredLecturers(List<Lecturer> filteredLecturers) {
        this.filteredLecturers = filteredLecturers;
    }
    
    
    public void addLecturer() {

        LecturerDAO lecturerDAO = new LecturerDAO();
        lecturerDAO.createLecturer(lecturer);
        lecturerList.clear();
        lecturerList = lecturerDAO.getLecturers();

        FacesMessage msg = new FacesMessage("Lecturer Added");
        FacesContext.getCurrentInstance().addMessage(null, msg);

    }

    public void onRowEdit(RowEditEvent event) {
        LecturerDAO lecturerDAO = new LecturerDAO();
        lecturerDAO.updateLecturer((Lecturer) event.getObject());

        FacesMessage msg = new FacesMessage("Lecturer Edited", ((Lecturer) event.getObject()).getLecturereName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Lecturer Edit Cancelled", ((Lecturer) event.getObject()).getLecturereName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowDelete() {
        LecturerDAO lecturerDAO = new LecturerDAO();
        lecturerDAO.deleteLecturer(selectedLecturer);
        lecturerList.clear();
        lecturerList = lecturerDAO.getLecturers();

        FacesMessage msg = new FacesMessage("Lecturer Deleted", selectedLecturer.getLecturereName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    
}
