/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd.ac.seu.my.controller;

import bd.ac.seu.my.DAO.CampusDAO;
import bd.ac.seu.my.DAO.FacultyDAO;
import bd.ac.seu.my.DAO.SchoolDAO;
import bd.ac.seu.my.model.Campus;
import bd.ac.seu.my.model.Faculty;
import bd.ac.seu.my.model.School;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Asaduzzaman Noor
 */
@ManagedBean
@SessionScoped
public class SchoolController implements Serializable {

    private School school;
    private Campus campus;
    private Faculty faculty;
    private School selectedSchool;
    private List<School> schoolList;
    private List<Campus> campusList;
    private List<Faculty> facultyList;
    private List<School> filteredSchools;
   
    public SchoolController() {
        campus = new Campus();
        faculty = new Faculty();
        school = new School();
        CampusDAO campusDAO = new CampusDAO();
        campusList = campusDAO.getCampuses();
        FacultyDAO facultyDAO = new FacultyDAO();
        facultyList = facultyDAO.getFaculties();
        SchoolDAO schoolDAO = new SchoolDAO();
        schoolList = schoolDAO.getSchools();
        filteredSchools = new ArrayList<>();
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public Campus getCampus() {
        return campus;
    }

    public void setCampus(Campus campus) {
        this.campus = campus;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public School getSelectedSchool() {
        return selectedSchool;
    }

    public void setSelectedSchool(School selectedSchool) {
        this.selectedSchool = selectedSchool;
    }

    public List<School> getSchoolList() {
        return schoolList;
    }

    public void setSchoolList(List<School> schoolList) {
        this.schoolList = schoolList;
    }

    public List<Campus> getCampusList() {
        return campusList;
    }

    public void setCampusList(List<Campus> campusList) {
        this.campusList = campusList;
    }

    public List<Faculty> getFacultyList() {
        return facultyList;
    }

    public void setFacultyList(List<Faculty> facultyList) {
        this.facultyList = facultyList;
    }

    public List<School> getFilteredSchools() {
        return filteredSchools;
    }

    public void setFilteredSchools(List<School> filteredSchools) {
        this.filteredSchools = filteredSchools;
    }

    public void addSchool() {

        SchoolDAO schoolDAO = new SchoolDAO();
        schoolDAO.createSchool(school);
        schoolList.clear();
        schoolList = schoolDAO.getSchools();

        FacesMessage msg = new FacesMessage("School Added");
        FacesContext.getCurrentInstance().addMessage(null, msg);

    }

    public void onRowEdit(RowEditEvent event) {
        SchoolDAO schoolDAO = new SchoolDAO();
        schoolDAO.updateSchool((School) event.getObject());

        FacesMessage msg = new FacesMessage("School Edited", ((School) event.getObject()).getSchoolName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("School Edit Cancelled", ((School) event.getObject()).getSchoolName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onRowDelete() {
        SchoolDAO schoolDAO = new SchoolDAO();
        schoolDAO.deleteSchool(selectedSchool);
        schoolList.clear();
        schoolList = schoolDAO.getSchools();

        FacesMessage msg = new FacesMessage("School Deleted", selectedSchool.getSchoolName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

}
